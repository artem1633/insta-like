const request = require('request');
const script = require('./functionAll');
const exec = require('executive');

/**
 * @return {boolean}
 */

async function DoLike(targetArr, accs, proxy) {

    global.try = true;

    let i = -1;
    global.count = 0;

    let response = [];

    // проверка на завершённость всех потоков что бы отправить единый ответ
    let timerId2 = await setInterval(function () {
        if (global.count <= 0 && global.try) {
            clearTimeout(timerId2);

            global.try = false;

            request.post({url:'https://like.teo-send.ru/api/like/log', form: {"result": response}}, function(err,httpResponse,body){
                console.log('Otvet:', body);
            });

            console.log('отправляем результаты', response);

            exec('pm2 restart 0');
        }
    }, 100);

    // запуски потоков
    let timerId = await setInterval(await async function () {
        if ((i < accs.length - 1)) {

            // количество потоков(---
            if (global.count < 3) {
                //-------------------)
                global.count++;
                global.try = true;
                i++;
                proxy.unshift(proxy.pop());
                response.push(await pullLike(targetArr, accs[i], proxy));
                global.count--;
                console.log('global.count:' + global.count);
            }

        } else {
            await clearTimeout(timerId);
        }
    }, 1000);

    global.try = false;
    return true;
}

// сама функция лайкера
function pullLike(targetArr, acc, proxy) {
    const puppeteer = require("puppeteer-extra");
    const pluginStealth = require("puppeteer-extra-plugin-stealth");
    puppeteer.use(pluginStealth());
    let userAgent = 'Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Mobile Safari/537.36';

////////////////////////////////////
    return (async () => {

        await script.get_line('./useragent.txt', await script.randomInteger(1, 3400), function (err, line) {
            userAgent = line;

        });

        let proxyOne = proxy[await script.randomInteger(0, proxy.length - 1)];
        const browser = await puppeteer.launch({
            headless: true,
            args: [
                '--no-sandbox',
                '--proxy-server=' + proxyOne
            ],
        });
        //новая вкладка
        const page = await browser.newPage();

        //переходим по адресу
        await page.emulate({
            viewport: {
                width: 720,
                height: 1480,
                isMobile: true,
                hasTouch: true
            },
            userAgent: userAgent
        });

        let login = acc.login;
        let pass = acc.password;

        // Логинимся
        let autho = await script.login(page, login, pass, proxyOne);

        if (autho !== '') {
            await console.log('proxyTempBan');
            await browser.close();
            return {
                acc: {
                    id: acc.id,
                    status: false,
                    comment: autho,
                },
            };
        } else {
            // лайкаем
            let res = await script.doLike(page, targetArr, proxyOne);

            await browser.close();
            return {
                acc: {
                    id: acc.id,
                    posts: res,
                },
            };
        }


    })();
}

module.exports = {
    DoLike: DoLike

};