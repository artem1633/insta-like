const fs = require('fs');
const request = require('request');

function get_line(filename, line_no, callback) {
    line_no = parseInt(line_no);
    let data = fs.readFileSync(filename, 'utf8');
    let lines = data.split("\n");
    for (let l in lines) {
        if (l == line_no - 1) {
            callback(null, lines[l].trim());
            return;
        }
    }
    throw new Error('File end reached without finding line');
}

function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}

function validateIpAndPort(inputProxy) {
    let parts = inputProxy.split(":");
    let ip = parts[0].split(".");
    let port = parts[1];
    return validateNum(port, 1, 65535) &&
        ip.length === 4 &&
        ip.every(function (segment) {
            return validateNum(segment, 0, 255);
        });
}

function validateNum(input, min, max) {
    let num = +input;
    return num >= min && num <= max && input === num.toString();
}

async function apiRequestOut(method, link, body) {

    if (method === 'post') {
        await request.post(link).form(body)
    }
    if (method === 'get') {
        await request.get(link)
    }
}

function apiRequestIn() {

    return new Promise(function (resolve, reject) {
        request('https://like.teo-send.ru/api/like/mission-instagram', function (error, res, body) {
            if (!error && res.statusCode === 200) {
                // console.log(body);
                resolve(body);
            } else {
                reject(error);
            }
        });
    });
}


async function login(page, login, password, proxyOne) {
    try {
        await page.goto('https://www.instagram.com/accounts/login/?next=%2Fstetsinat%2F&source=mobile_nav', {
            waitUntil: 'domcontentloaded',
            timeout: 5000
        })

    } catch (e) {
        try {
            await page.reload();
            // await page.screenshot({path: 'failedloadpage1.png'});
        } catch (e) {

        }
        try {

            await page.goto('https://www.instagram.com/accounts/login/?next=%2Fstetsinat%2F&source=mobile_nav', {
                waitUntil: 'domcontentloaded',
                timeout: 5000
            })

        } catch (e) {
            try {
                await page.goBack;
                await page.waitFor(1000);
                await page.goto('https://www.instagram.com/accounts/login/?next=%2Fstetsinat%2F&source=mobile_nav', {
                    waitUntil: 'domcontentloaded',
                    timeout: 5000
                });
            } catch (e) {
                await page.screenshot({path: 'failedloadpage2.png'});

                return {error: 'failed load page ' + proxyOne}
            }
        }
    }
    try {
        await page.waitForSelector('.f0n8F', [{timeout: 5000}]);
    } catch (e) {
        return {error: 'authorization failed'}
    }

    await console.log('start login');

    try {
        await page.waitFor(1000);

        const loginInputId = await page.evaluate(() => {
            return document.getElementsByClassName('f0n8F')[0].lastChild.id
        });

        const passwordInputId = await page.evaluate(() => {
            return document.getElementsByClassName('f0n8F')[1].lastChild.id
        });


        // await console.log('keyboard');
        await console.log(login + ' : ' + password);
        await page.focus('#' + loginInputId);
        await page.keyboard.type(login);

        await page.focus('#' + passwordInputId);
        await page.keyboard.type(password);

    } catch (e) {
        await console.log('false load page');
    }

    await page.keyboard.press('Enter');
    await page.waitFor(4000);

    try {
        if (await page.$('#slfErrorAlert') !== null) {
            return {error: 'proxyTempBan'};
        }
    } catch (e) {

    }
    try {
        await page.waitForSelector('.glyphsSpriteUser__outline__24__grey_9.u-__7', {timeout: 1000});
        await console.log('finish');
        return '';
    } catch (e) {
        await page.screenshot({path: 'errorautorization.png'});
        return {error: 'error autorization'};

    }


}

async function doLike(page, linkArr, proxyOne) {

    let responseTargetArr = [];

    // перебираем посты
    for (let i = 0; i < linkArr.length; i++) {

        // отлавливаем возможные ошибки на каждом из этапов
        try {
            await page.goto(linkArr[i].link, {waitUntil: 'domcontentloaded', timeout: 6000});
        } catch (e) {
            await responseTargetArr.push([
                linkArr[i].id,
                false,
                'failed load post',
                proxyOne
            ]);
            console.log('href:', await page.evaluate(() => location.href));
            await page.screenshot({path: 'failedloadpost.png'});
            continue;
        }

        try {
            await page.waitForSelector('#react-root > section > main > div > div > article > div.eo2As > section.ltpMr.Slqrh > span.fr66n > button', {timeout: 3000});
        } catch (e) {
            try {
                await page.waitForSelector('body > div > div.page.-cx-PRIVATE-Page__body.-cx-PRIVATE-Page__body__ > div > div > p > a', {timeout: 500});
                await responseTargetArr.push([
                    linkArr[i].id,
                    false,
                    'error page',
                    proxyOne
                ]);
                continue;
            } catch (e) {
                await responseTargetArr.push([
                    linkArr[i].id,
                    false,
                    'not found post selector',
                    proxyOne
                ]);
                await page.screenshot({path: 'notfoundpostselector.png'});
                continue;
            }
        }

        await page.waitFor(1000);

        try {
            await page.waitForSelector('.glyphsSpriteHeart__filled__24__red_5.u-__7', {timeout: 500});
        } catch (e) {
            try {

                // жмём лайк
                await page.click('button > span.glyphsSpriteHeart__outline__24__grey_9.u-__7');
                try {

                    // проверяем есть ли он и если есть забираем количество трёма способами от самого частого к редкому
                    await page.waitForSelector('.glyphsSpriteHeart__filled__24__red_5.u-__7', {timeout: 2000});
                } catch (e) {
                    responseTargetArr.push([
                        linkArr[i].id,
                        false,
                        'didnt like',
                        proxyOne
                    ]);
                    console.log('href:', await page.evaluate(() => location.href));
                    await page.screenshot({path: 'didntlike.png'});
                }
            } catch (e) {
                responseTargetArr.push([
                    linkArr[i].id,
                    false,
                    'error like',
                    proxyOne
                ]);
                console.log('href:', await page.evaluate(() => location.href));
                await page.screenshot({path: 'errorlike.png'});
            }
        }

        try {
            const element = await page.$("#react-root > section > main > div > div > article > div.eo2As > section.EDfFK.ygqzn > div > div > a > span");
            let arr = [
                linkArr[i].id,
                true,
                await page.evaluate(element => element.textContent, element)
            ];
            responseTargetArr.push(arr);
        } catch (e) {
            try {
                const element = await page.$("#react-root > section > main > div > div > article > div.eo2As > section.EDfFK.ygqzn > div > div > a");
                let countStr = await page.evaluate(element => element.textContent, element);
                responseTargetArr.push([
                    linkArr[i].id,
                    true,
                    parseInt(countStr.replace(/\D+/g, ""))
                ]);
            } catch (e) {
                try {
                    await page.click('#react-root > section > main > div > div > article > div.eo2As > section.EDfFK.ygqzn > div > span');
                    await page.waitForSelector('#react-root > section > main > div > div > article > div.eo2As > section.EDfFK.ygqzn > div > div > div.vJRqr > span', {timeout: 2000});
                    const element1 = await page.$("#react-root > section > main > div > div > article > div.eo2As > section.EDfFK.ygqzn > div > div > div.vJRqr > span");
                    responseTargetArr.push([
                        linkArr[i].id,
                        true,
                        await page.evaluate(element1 => element1.textContent, element1)
                    ]);
                } catch (e) {
                    responseTargetArr.push([
                        linkArr[i].id,
                        false,
                        'не найдено количество',
                        proxyOne
                    ]);
                }
            }
        }
    }
    console.log('responseTargetArr: ', responseTargetArr);
    console.log('returnFunAll');
    return responseTargetArr;
}

module.exports = {
    get_line: get_line,
    randomInteger: randomInteger,
    apiRequestOut: apiRequestOut,
    login: login,
    doLike: doLike,
    validateIpAndPort: validateIpAndPort,
    apiRequestIn: apiRequestIn,
};