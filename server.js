const express = require('express');
const app = express();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const script = require('./functionAll');
const doLike = require('./doLike');
const os = require('os');
const exec = require('executive');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// мониторит работу процесора и перезапускает если средняя нагрузка на проц меньше 1% в 5 минут
setInterval(function () {
    let proces = os.loadavg();
    console.log('proc:', proces);
    if(proces[1] < 0.9 ){
        exec('pm2 restart 0');
    }

}, 300000);

// флаг запрещающий бесконечный запуск
global.try = false;

// стук каждые 15с на наявность миссий
setInterval(async function () {
    if (global.try !== true) {

        // запрос данных
        let obj = await script.apiRequestIn();
        obj = await JSON.parse(obj);

        // проверки на пустоту приходящих данных
        if (!obj.proxy || await script.validateIpAndPort(obj.proxy[0]) === false) {
            try{
                await script.apiRequestOut('post', 'https://like.teo-send.ru/api/like/mission-instagram', JSON.stringify({error: obj.proxy[0] + ' - not proxy!'}));
            }catch (e) {
                await console.log('Error DATA:', obj);
            }
        } else {
            if (obj.mission[0].link === 'https://www.instagram.com/p/') {
                await script.apiRequestOut('post', 'https://like.teo-send.ru/api/like/mission-instagram', JSON.stringify({error: obj.link + ' - invalid link!'}));
            } else {
                if (obj.mission[0].type === 'ins_like') {

                    // запуск осн. функции
                    await doLike.DoLike(
                        obj.mission, // number
                        obj.account, // Arr
                        obj.proxy // string
                    );
                }
            }
        }
    }

}, 15000);

// запуск сервера
http.listen(3002, function () {
    console.log(`app listening on port 3002!`);
});


